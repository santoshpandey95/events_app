<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        // Store Default User in database for login
        $user = new User();

        $password = $this->passwordHasher->hashPassword($user, '1234');

        $user->setEmail('admin@admin.com');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }
}
