<?php

namespace App\Controller;

use App\Entity\Event;
use App\Form\EventFormType;
use App\Repository\EventRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EventController extends AbstractController
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    public function __construct(EventRepository $eventRepository){
        $this->eventRepository = $eventRepository;
    }

    #[Route('/events', name: 'events_index', methods: ['GET'])]
    public function index(Request $request, ): Response
    {
        $queryBuilder = $this->eventRepository->createQueryBuilder("e");

        $type = $request->query->get('type');

        if($type == 'finished'){
            $now = new \DateTime();
            $queryBuilder->where('e.endDate < :now')
                ->setParameter('now', $now->format('Y-m-d'));
        }

        if($type == 'upcomming'){
            $now = new \DateTime();
            $queryBuilder->where('e.startDate >= :now')
                ->setParameter('now', $now->format('Y-m-d'));
        }

        // events by ascending order
        $events = $queryBuilder->orderBy('e.startDate', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('events/index.html.twig', [
            'type' => $request->query->get('type', 'all'),
            'events' => $events
        ]);
    }

    #[Route('/events/create', name: 'events_create')]
    public function create(Request $request, ManagerRegistry $doctrine): Response
    {
        $event = new Event();

        $form = $this->createForm(EventFormType::class, $event, [
            'attr' => ['id' => 'eventCreateForm']
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $doctrine->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            $this->addFlash('success', 'Event added successfully!');

            return $this->redirectToRoute('events_index');
        }

        return $this->render('events/create.html.twig', [
            'event_form' => $form->createView()
        ]);
    }

    #[Route('/events/{id}/edit', name: 'events_edit')]
    public function edit(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $event = $this->eventRepository->find($id);

        if(!$event){
            throw new NotFoundHttpException('Sorry! The event your are looking for does not exist');
        }

        $form = $this->createForm(EventFormType::class, $event, [
            'attr' => ['id' => 'eventCreateForm']
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $doctrine->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            $this->addFlash('success', 'Event updated successfully!');

            return $this->redirectToRoute('events_index');
        }

        return $this->render('events/edit.html.twig', [
            'event_form' => $form->createView(),
            'event' => $event
        ]);
    }

    #[Route('/events/{id}/delete', name: 'events_delete', methods: ['POST'])]
    public function delete(int $id, ManagerRegistry $doctrine): Response
    {
        $event = $this->eventRepository->find($id);

        $entityManager = $doctrine->getManager();
        $entityManager->remove($event);
        $entityManager->flush();

        return new Response(json_encode([
            'status' => true
        ]));
    }


}
